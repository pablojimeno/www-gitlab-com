---
layout: handbook-page-toc
title: "OneTrust"
description: "OneTrust is privacy, security, and data governance software that marketing uses as our privacy and compliance solution on our websites."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Uses

OneTrust is privacy, security, and data governance software that marketing uses as our privacy and compliance solution on our websites. The marketing operations team works closely with our legal team and is primarily responsible for our privacy and compliance on our websites including cookie preferences. 

## Support

1. Technical assistance: Slack [#mktgops](https://gitlab.slack.com/archives/mktgops)
1. [Support Portal](https://support.onetrust.com/hc/en-us) (requires seperate account/login)
1. `support@onetrust.com`

## Implementation

[See the epic](https://gitlab.com/groups/gitlab-com/-/epics/1265) for more information.
