---
layout: handbook-page-toc
title: External Virtual Events
description: An overview of external virtual events including virtual conferences where we sponsor a booth, and sponsored webcasts with third party vendors.
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# External Virtual Events Overview
{:.no_toc}
---

