features:
  primary:
    - name: "Maintenance Mode"
      available_in: [premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/administration/maintenance_mode/index.html'
      image_url: '/images/unreleased/maintenance_mode.png'
      reporter: fzimmer
      stage: enablement
      categories:
        - "Disaster Recovery"
      epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/2149'
      description: |
        Systems administrators regularly perform maintenance operations on their GitLab instance to keep it performing optimally. Occasionally, you need to do tasks that can't be easily completed while your users are also making changes to the system. For example, you may need to perform a [planned failover to a secondary site](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/planned_failover.html) as part of the company's business continuity plan, and you need to ensure the secondary is fully synchronized first. Until GitLab 13.8, you could [restrict users from logging in](https://docs.gitlab.com/omnibus/maintenance/#restrict-users-from-logging-into-gitlab), but this would block the entire UI and would render GitLab inaccessible to users.
        
        GitLab 13.9 introduces [maintenance mode](https://docs.gitlab.com/ee/administration/maintenance_mode/index.html), where write operations are disabled at the application level. This means that GitLab is effectively in a read-only state for all non-administrative users (administrators are still able to edit application settings and [background jobs continue](https://docs.gitlab.com/ee/administration/maintenance_mode/index.html#background-jobs)). Regular users are able to log in to GitLab, view the interface and perform other read-only operations, such as `git clone` or `git pull`. Using maintenance mode, systems administrators can perform maintenance operations, such as failing over to a secondary site, with minimal disruption to regular users.

        Note that GitLab already [supports zero-downtime updates](https://docs.gitlab.com/omnibus/update/#zero-downtime-updates) and enabling maintenance mode is not required to keep your instance up-to-date.
